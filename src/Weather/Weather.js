import React from 'react';
import {Component} from 'react'
import axios from 'axios';
import '../App.css';
import ToggleDisplay from 'react-toggle-display';



class Weather extends Component {
    constructor() {
        super();
        this.state = {
            show: false
        };
        let updateWeather = (result) => {
            this.setState({
                cityName: result.data.name,
                temperatureC: result.data.main.temp,
                temperatureFar: (((result.data.main.temp)*9)/5)+32,
                pressure: result.data.main.pressure,
                humidity: result.data.main.humidity,
                icon: result.data.weather[0].icon,
                skyDescription: result.data.weather[0].description,
                sthWithCloud: result.data.weather[0].main,
                country: result.data.sys.country
            });
        };

        let getGeolocationData = () => {
            return getGeolocationPromise()
                .then((position) => axios
                    .get(geoPositionUrl(position)))
                .then((result) => updateWeather(result))
                .catch((err) => alert('NotWorking'))
        };

        let geoPositionUrl = (position) => {
            return `https://fcc-weather-api.glitch.me/api/current?lat=${position.coords.latitude}&lon=${position.coords.longitude} `
        };

        let getGeolocationPromise = () => {
            return new Promise((resolve, reject) => {
                window.navigator.geolocation.getCurrentPosition(
                    (position) => resolve(position),
                    (err) => reject(err)
                )
            })
        };

        getGeolocationData();
    }

    changeOfDegree(){
        this.setState({show: !this.state.show})

    }

    render() {
        if (!this.state) {
            return null;
        }
        return (
           <div>
               <h1>Weather App</h1>
               <div className="decoratorHeader">Check our weather Service </div>
            <div className="weatherApp">
                <div className="weatherData">
                    <div className="key">City Name / Country:</div>
                    <div className="value">{this.state.cityName}/{this.state.country}</div>
                    <div className="key">Temperature:</div>
                    <div className="value">
                        <ToggleDisplay show={!this.state.show}>{this.state.temperatureC}°C</ToggleDisplay>
                        <ToggleDisplay if={this.state.show}>{this.state.temperatureFar}F</ToggleDisplay>

                        <button className="changingTempButton" onClick={()=>this.changeOfDegree()}>°C/F</button></div>
                    <div className="key">Pressure:</div>
                    <div className="value">{this.state.pressure} hPa</div>
                    <div className="key">Humidity:</div>
                    <div className="value">{this.state.humidity}%</div>
                    <div className="key">Humidity:</div>

                </div>
                <div className="weatherImg">
                    <img src={this.state.icon}/>
                    <div>{this.state.skyDescription}</div>
                </div>
            </div>
           </div>
        );
    }
}

export default Weather;

